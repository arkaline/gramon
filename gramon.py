# -*- coding: utf-8 -*-
# see http://aoo-extensions.sourceforge.net/en/project/watchingwindow
import uno
import unohelper

from com.sun.star.awt.PosSize import (POSSIZE as PS_POSSIZE)
from com.sun.star.awt.MessageBoxType import MESSAGEBOX
from com.sun.star.awt.MessageBoxButtons import BUTTONS_OK

IMPL_NAME = "lo.org.gramon.gramon"
RESOURCE_NAME = "private:resource/toolpanel/lo.org.gramon/gramon"

def create_uno_struct(cTypeName):
    """Create a UNO struct and return it.
    Similar to the function of the same name in OOo Basic. -- Copied from Danny Brewer library
    """
    sm = uno.getComponentContext().ServiceManager
    oCoreReflection = sm.createInstance( "com.sun.star.reflection.CoreReflection" )
    # Get the IDL class for the type name
    oXIdlClass = oCoreReflection.forName( cTypeName )
    # Create the struct.
    __oReturnValue, oStruct = oXIdlClass.createObject( None )
    return oStruct

def create_control(ctx, control_type, x, y, width, height, names, values):
    """ create a control. """
    smgr = ctx.getServiceManager()
    
    ctrl = smgr.createInstanceWithContext("com.sun.star.awt." + control_type, ctx)
    ctrl_model = smgr.createInstanceWithContext("com.sun.star.awt." + control_type + "Model", ctx)
    
    if len(names) > 0:
        ctrl_model.setPropertyValues(names, values)
    ctrl.setModel(ctrl_model)
    ctrl.setPosSize(x, y, width, height, PS_POSSIZE)
    return ctrl


def create_container(ctx, parent, names, values, __fit=True):
    """ create control container. """
    cont = create_control(ctx, "UnoControlContainer", 0, 0, 0, 0, names, values)
    cont.createPeer(parent.getToolkit(), parent)
    #if fit:
    #    cont.setPosSize()
    return cont

def get_backgroundcolor(window):
    """ Get background color through accesibility api. """
    try:
        return window.getAccessibleContext().getBackground()
    except:
        pass
    return 0xeeeeee

"""

"""
import threading

from com.sun.star.awt import (XWindowListener, XActionListener, XMouseListener)
from com.sun.star.ui import XUIElement, XToolPanel
from com.sun.star.ui.UIElementType import TOOLPANEL as UET_TOOLPANEL
from com.sun.star.lang import XComponent

class GramonModel(unohelper.Base, XUIElement, XToolPanel, XComponent):
    """ Gramon model. """
    
    def __init__(self, ctx, frame, parent):
        self.ctx = ctx
        self.frame = frame
        self.parent = parent
        
        self.view = None
        self.window = None
        try:
            view = GramonView(ctx, self, frame, parent)
            self.view = view
            self.window = view.cont
            
            def _focus_back():
                self.frame.getContainerWindow().setFocus()
            
            threading.Timer(0.3, _focus_back).start()
        except Exception as e:
            print(e)
    
    # XComponent
    def dispose(self):
        self.ctx = None
        self.frame = None
        self.parent = None
        self.view = None
        self.window = None
    
    def addEventListener(self, ev): pass
    def removeEventListener(self, ev): pass
    
    # XUIElement
    def getRealInterface(self):
        return self
    @property
    def Frame(self):
        return self.frame
    @property
    def ResourceURL(self):
        return RESOURCE_NAME
    @property
    def Type(self):
        return UET_TOOLPANEL
    
    # XToolPanel
    def createAccessible(self, __parent):
        return self.window.getAccessibleContext()
    @property
    def Window(self):
        return self.window
    
    def dispatch(self, cmd, args):
        """ dispatch with arguments. """
        helper = self.ctx.getServiceManager().createInstanceWithContext("com.sun.star.frame.DispatchHelper", self.ctx)
        helper.executeDispatch(self.frame, cmd, "_self", 0, args)
    
    def hidden(self):
        self.data_model.enable_update(False)
    def shown(self):
        self.data_model.enable_update(True)

from com.sun.star.awt.MouseButton import LEFT as MB_LEFT

class GramonView(unohelper.Base, XWindowListener, XActionListener, XMouseListener):
    """ Gramon view. """
    LR_MARGIN = 3
    TB_MARGIN = 3
    BUTTON_SEP = 2
    BUTTON_SZ = 50
    
    def __init__(self, ctx, model, frame, parent):
        self.model = model
        self.parent = parent
        self.controller = frame.getController()
        self.ctx = ctx
        self.width = 200
        self.height = 500
        self.lbtn = []
        
        try:
            self._create_view(ctx, parent)
        except Exception as e:
            print(("Failed to create Gramon view: %s" % e))
        parent.addWindowListener(self)
    
    def _create_view(self, ctx, parent):
        LR_MARGIN = self.LR_MARGIN
        TB_MARGIN = self.TB_MARGIN
        BUTTON_SEP = self.BUTTON_SEP
        
        cont = create_container(ctx, parent, ("BackgroundColor",), (get_backgroundcolor(parent),))
        self.cont = cont
        smgr = ctx.getServiceManager()
        self.smgr = smgr
        self.ctx = ctx
        
        # création des boutons pour les fonctions sélectionnées
        BUTTON_SZ = self.BUTTON_SZ
        WIDTH = self.width
        HEIGHT = self.height
        posX = LR_MARGIN
        posY = TB_MARGIN
        for fct in ['verbe','nom','pronom','adjectif','determinant','preposition','adverbe','conjonction']:
            btn = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlImageControl', ctx)        
            btn_model = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlImageControlModel', ctx)
            img = "vnd.sun.star.extension://"+IMPL_NAME+"/images/"+fct+".png"
            btn_model.setPropertyValues( ('ImageURL','ScaleImage','ScaleMode','HelpText','Name',),
                        (img,True,1,fct,fct,) )
            btn.setModel(btn_model)
            btn.setPosSize(posX, posY, BUTTON_SZ, BUTTON_SZ, PS_POSSIZE)
            cont.addControl(fct, btn)
            btn.addMouseListener(self)
            posX += (BUTTON_SZ+BUTTON_SEP)
            if (posX+BUTTON_SZ) > WIDTH:
                posY += (BUTTON_SZ+BUTTON_SEP)
                posX = LR_MARGIN
            self.lbtn.append(btn)

        posMaxY = HEIGHT-TB_MARGIN-BUTTON_SZ
        self.edbtn = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlButton', ctx)        
        btn_model = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlButtonModel', ctx)
        btn_model.setPropertyValues( ('Label',), ("Corriger",) )
        self.edbtn.setModel(btn_model)
        self.edbtn.setPosSize(LR_MARGIN, posMaxY, 64, 32, PS_POSSIZE)
        cont.addControl("correction", self.edbtn)
        self.edbtn.setActionCommand("correction")
        self.edbtn.addActionListener(self)

        self.plusbtn = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlButton', ctx)        
        btn_model = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlButtonModel', ctx)
        btn_model.setPropertyValues( ('Label','HelpText',), ("+","espacer les lignes",) )
        self.plusbtn.setModel(btn_model)
        self.plusbtn.setPosSize(LR_MARGIN+16+BUTTON_SEP, posMaxY, 32, 32, PS_POSSIZE)
        cont.addControl("plusesplignes", self.plusbtn)
        self.plusbtn.setActionCommand("plusesplignes")
        self.plusbtn.addActionListener(self)

        self.moinsbtn = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlButton', ctx)        
        btn_model = smgr.createInstanceWithContext('com.sun.star.awt.UnoControlButtonModel', ctx)
        btn_model.setPropertyValues( ('Label','HelpText',), ("-","réduire l'espacement des lignes",) )
        self.moinsbtn.setModel(btn_model)
        self.moinsbtn.setPosSize(LR_MARGIN+64+2*BUTTON_SEP+32, posMaxY, 32, 32, PS_POSSIZE)
        cont.addControl("moinsesplignes", self.moinsbtn)
        self.moinsbtn.setActionCommand("moinsesplignes")
        self.moinsbtn.addActionListener(self)

    def _update_view(self):
        LR_MARGIN = self.LR_MARGIN
        TB_MARGIN = self.TB_MARGIN
        BUTTON_SEP = self.BUTTON_SEP
        BUTTON_SZ = self.BUTTON_SZ
        HEIGHT = self.height

        # mie à jour des boutons d'après les fonctions sélectionnées
        ps = self.edbtn.getPosSize()
        posMaxY = HEIGHT-TB_MARGIN-ps.Height
        self.edbtn.setPosSize(LR_MARGIN, posMaxY, ps.Width, ps.Height, PS_POSSIZE)
        self.plusbtn.setPosSize(LR_MARGIN+ps.Width+BUTTON_SEP, posMaxY, 32, 32, PS_POSSIZE)
        ps = self.edbtn.getPosSize()
        self.moinsbtn.setPosSize(LR_MARGIN+ps.Width+2*BUTTON_SEP+32, posMaxY, 32, 32, PS_POSSIZE)

        posX = LR_MARGIN
        posY = TB_MARGIN
        for btn in self.lbtn:
            ps = btn.getPosSize()
            btn.setVisible(True)
            btn.setPosSize(posX, posY, BUTTON_SZ, BUTTON_SZ, PS_POSSIZE)

            posX += (BUTTON_SZ+BUTTON_SEP)
            if (posX+BUTTON_SZ) > self.width:
                posX = LR_MARGIN
                posY += (BUTTON_SZ+BUTTON_SEP)

    # XEventListener
    def disposing(self, __ev):
        self.cont = None
        self.model = None
    
    # XWindowListener
    def windowHidden(self, ev): pass
    def windowShown(self, ev): pass
    def windowMoved(self, ev): pass
    def windowResized(self, ev):
        ps = ev.Source.getPosSize()
        self.width = ps.Width
        self.height = ps.Height
        self._update_view()
    
    # XMouseListener
    def mouseEntered(self, ev):
        btn_model = ev.Source.Model
        btn_model.setPropertyValues( ('BackgroundColor',), (0xffffff,) )
    def mouseExited(self, ev):
        btn_model = ev.Source.Model
        btn_model.setPropertyValues( ('BackgroundColor',), (get_backgroundcolor(self.parent),) )
    def mousePressed(self, ev):
        if ev.Buttons == MB_LEFT and ev.ClickCount == 1:
            img = ev.Source.Model.ImageURL
            ##self.messagebox(img)
            desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
            xDocument = desktop.getCurrentComponent()
            marqueur_gramon(xDocument, img)

    def mouseReleased(self, ev): pass

    # XActionListener
    def actionPerformed(self, ev):
        cmd = ev.ActionCommand
        try:
            desktop = self.ctx.ServiceManager.createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
            xDocument = desktop.getCurrentComponent()
            xTextRange = get_text_range(xDocument)
            if xTextRange == None:
                return False

            if cmd == 'correction':
                oPage = xDocument.DrawPage
                for cursor in xTextRange:
                    mots = cursor.getString().split() # extraire des étiquettes des mots"
                    shapesup=[]
                    nNumShapes = oPage.getCount()
                    for x in range (nNumShapes): # toutes les formes de la page
                        oShape = oPage.getByIndex(x)
                        if oShape.Title in mots:
                            shapesup.append(oShape)

                    for oShape in shapesup:
                        oPage.remove(oShape)
                    del shapesup
                del xTextRange
                return True
            if cmd == "plusesplignes":
                for cursor in xTextRange:
                    args = cursor.getPropertyValue('ParaLineSpacing')
                    args.Mode = 0 # proportionnel http://www.openoffice.org/api/docs/common/ref/com/sun/star/style/LineSpacingMode.html
                    args.Height *= 1.05
                    args.Height = int(args.Height)
                    cursor.setPropertyValue('ParaLineSpacing', args)
                del xTextRange
                return True
            if cmd == "moinsesplignes":
                for cursor in xTextRange:
                    args = cursor.getPropertyValue('ParaLineSpacing')
                    args.Mode = 0 # proportionnel http://www.openoffice.org/api/docs/common/ref/com/sun/star/style/LineSpacingMode.html
                    args.Height *= 0.95
                    args.Height = int(args.Height)
                    cursor.setPropertyValue('ParaLineSpacing', args)
                del xTextRange
                return True
        except Exception as e:
            print(e)

"""

"""
from com.sun.star.ui import XUIElementFactory

class GramonFactory(unohelper.Base, XUIElementFactory):
    """ Factory for Gramon """
    def __init__(self, ctx):
        self.ctx = ctx
    
    # XUIElementFactory
    def createUIElement(self, name, args):
        element = None
        if name == RESOURCE_NAME:
            frame = None
            parent = None
            for arg in args:
                if arg.Name == "Frame":
                    frame = arg.Value
                elif arg.Name == "ParentWindow":
                    parent = arg.Value
            if frame and parent:
                try:
                    element = GramonModel(self.ctx, frame, parent)
                except Exception as e:
                    print(e)
        return element
    
    # XServiceInfo
    def getImplementationName(self):
        return IMPL_NAME
    def supportsService(self, name):
        return IMPL_NAME == name
    def supportedServiceNames(self):
        return (IMPL_NAME,)

g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(
    GramonFactory, IMPL_NAME, (IMPL_NAME,),)

"""

"""
def marqueur_gramon(xDocument, marq='verbe'):
    try:
        xTextRange = get_text_range(xDocument)
        if xTextRange == None:
            return False
        for cursor in xTextRange:
            args = cursor.getPropertyValue('ParaLineSpacing')
            args.Mode = 0 # proportionnel http://www.openoffice.org/api/docs/common/ref/com/sun/star/style/LineSpacingMode.html
            if args.Height < 120:
                args.Height = 200
                cursor.setPropertyValue('ParaLineSpacing', args)
            
            marque_symbole(xDocument, marq, cursor)
        del xTextRange
        return True
    except:
        return False

"""

"""
def marque_symbole(xDocument, img, cursor):
    from com.sun.star.text.TextContentAnchorType import AT_CHARACTER
    from com.sun.star.text.WrapTextMode import THROUGHT
    from com.sun.star.text.RelOrientation import CHAR
    from com.sun.star.text.HoriOrientation import CENTER
    from com.sun.star.text.VertOrientation import LINE_TOP

    txt_phon = cursor.getString()
    cursor.collapseToStart()
    cursor.goRight(int(len(txt_phon)/2), False)

    pip = uno.getComponentContext().getValueByName("/singletons/com.sun.star.deployment.PackageInformationProvider")
    url = pip.getPackageLocation(IMPL_NAME)
    fimgname = url+img[len("vnd.sun.star.extension://"+IMPL_NAME):]
    hh = cursor.getPropertyValue("CharHeight")

    sz = uno.createUnoStruct("com.sun.star.awt.Size")
    sz.Width = sz.Height = hh*40
    oShape = make_shape(xDocument, "com.sun.star.drawing.GraphicObjectShape")
    oShape.GraphicURL = fimgname
    oShape.Title= txt_phon
    oShape.Size = sz
    oShape.FillTransparence = 100
    oShape.LineTransparence = 100

    # ces lignes sont à placer AVANT le "insertTextContent" pour que la forme soit bien configurée
    oShape.AnchorType = AT_CHARACTER # com.sun.star.text.TextContentAnchorType.AT_CHARACTER
    oShape.HoriOrient = CENTER # com.sun.star.text.HoriOrientation.LEFT
    oShape.HoriOrientRelation = CHAR # com.sun.star.text.RelOrientation.CHAR
    oShape.VertOrient = LINE_TOP # com.sun.star.text.VertOrientation.CHAR_BOTTOM
    oShape.VertOrientRelation = CHAR # com.sun.star.text.RelOrientation.CHAR

    # insertion de la forme dans le texte à la position du curseur
    cursor.getText().insertTextContent(cursor, oShape, False)

    # cette ligne est à placer APRÈS le "insertTextContent" qui écrase la propriété
    oShape.TextWrap = THROUGHT
    
    # replacement sauvage pour que le symbole soit au-dessus du mot
    point = oShape.getPosition()
    point.Y += int(2.2*oShape.Size.Height)
    oShape.setPosition(point)

"""

"""
def get_text_range(xDocument):
    """
        Récupère le textRange correspondant au mot sous le curseur ou à la sélection
    """

    if not xDocument.supportsService("com.sun.star.text.TextDocument"):
        return None

    #the writer controller impl supports the css.view.XSelectionSupplier interface
    xSelectionSupplier = xDocument.getCurrentController()
    xIndexAccess = xSelectionSupplier.getSelection()

    if xIndexAccess.supportsService("com.sun.star.text.TextTableCursor"):
        ##return getXCellTextRange(xDocument, xIndexAccess)
        return None

    xTextRanges = []
    count = 0
    try:
        count = xIndexAccess.getCount()
    except:
        return None

    for i in range(count):
        xTextRange = xIndexAccess.getByIndex(i)
        xText = xTextRange.getText() ## get the XText interface
        # sélection du mot courant
        xWordCursor = xText.createTextCursorByRange(xTextRange)
        xWordCursor.gotoStartOfWord(False)
        xWordCursor.gotoEndOfWord(True)
        xTextRanges.append(xWordCursor)
    return xTextRanges

def make_shape(oDoc, cShapeClassName, oPosition=None, oSize=None):
    """Create a new shape of the specified class.
    Position and size arguments are optional.
    """
    oShape = oDoc.createInstance(cShapeClassName)

    if oPosition != None:
        oShape.Position = oPosition
    if oSize != None:
        oShape.Size = oSize

    return oShape

def messagebox(message, titre="Message"):
    ctx = uno.getComponentContext()
    smgr = ctx.getServiceManager()
    win = smgr.createInstanceWithContext("com.sun.star.frame.Desktop", ctx).getCurrentFrame().getContainerWindow()
    toolkit = win.getToolkit()
    box = toolkit.createMessageBox(win, MESSAGEBOX, BUTTONS_OK, titre, message)
    return box.execute()

"""

"""
if __name__ == "__main__":
    # get the uno component context from the PyUNO runtime
    localContext = uno.getComponentContext()

    # create the UnoUrlResolver
    resolver = localContext.ServiceManager.createInstanceWithContext(
                "com.sun.star.bridge.UnoUrlResolver", localContext )

    # connect to the running office
    ctx = resolver.resolve( "uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext" )
    smgr = ctx.ServiceManager

    # get the central desktop object
    desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)

    # access the current writer document
    xDocument = desktop.getCurrentComponent()
    

